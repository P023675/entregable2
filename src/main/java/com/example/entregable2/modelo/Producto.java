package com.example.entregable2.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Producto {

    //public long id;
    @Id
    public String id;
    public String nombre;
    public double precio;
    public double cantidad;

    // Jackson - Serializador/Deserializador JSON usado por Spring
    @JsonIgnore
    public List<String> idsVendedores= new ArrayList<>();
}
