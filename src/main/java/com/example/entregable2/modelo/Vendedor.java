package com.example.entregable2.modelo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class Vendedor {
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    //public long id;
    @Id
    public String id;

    public String nombre;
    public String direccion;
    public double sueldo;
}
