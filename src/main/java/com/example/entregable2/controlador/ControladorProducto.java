package com.example.entregable2.controlador;

import com.example.entregable2.modelo.Producto;
import com.example.entregable2.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.v1}/productos/{idProducto}")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    //public Producto obtenerProducto(@PathVariable long idProducto){
    public ResponseEntity<Producto> obtenerProducto(@PathVariable String idProducto){
        /*
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

         */

        try{
            return ResponseEntity.ok(buscarProductoPorId(idProducto));
        }catch (ResponseStatusException x){
            return ResponseEntity.notFound().build();
        }

        //return buscarProductoPorId(idProducto);
    }


    public static class ProductoEntrada{
        public String nombre;
        public Double precio;
        public Double cantidad;
    }

    @PutMapping
    public void reemplazarProducto(@PathVariable(name = "idProducto") String id,
                                   @RequestBody ProductoEntrada p){

        /*
        final Producto productoExistente = this.servicioProducto.obtenerProducto(id);
        if(productoExistente == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
         */

        final Producto x = buscarProductoPorId(id);

        if(p.nombre == null || p.nombre.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.precio == null || p.precio <= 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.cantidad == null || p.cantidad <0.0 )
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.nombre = p.nombre;
        x.precio = p.precio;
        x.cantidad = p.cantidad;
        //p.id = id;
        this.servicioProducto.reemplazarProducto(id,x);
    }

    @PatchMapping
    public void modificarProducto(@PathVariable String idProducto,
                                  @RequestBody ProductoEntrada p){
        final Producto x = buscarProductoPorId(idProducto);
        if(p.nombre != null){
            if(p.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.nombre = p.nombre;
        }
        if(p.precio != null) {
            if(p.precio <= 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.precio = p.precio;
        }
        if(p.cantidad != null){
            if(p.cantidad < 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.cantidad = p.cantidad;
        }

        this.servicioProducto.reemplazarProducto(idProducto,x);
    }

    @DeleteMapping
    public void borrarProducto(@PathVariable(name  = "idProducto") String id){
           this.servicioProducto.eliminarProducto(id);
    }

    private Producto buscarProductoPorId(String idProducto){
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return p;
    }

}
