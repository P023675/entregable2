package com.example.entregable2.controlador;

import com.example.entregable2.modelo.Producto;
import com.example.entregable2.servicio.ServicioProducto;
import com.example.entregable2.servicio.impl.ServicioProductoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/productos")
public class ControladorListaProductos {

    @Autowired
    ServicioProducto servicioProducto;

    @PostMapping
    public void agregarProducto(@RequestBody Producto p){
        this.servicioProducto.agregarProducto(p);
    }

    @GetMapping
    public List<Producto> obtenerProductos(){
        return this.servicioProducto.obtenerProductos();
    }

}
