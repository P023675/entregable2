package com.example.entregable2.controlador;

import com.example.entregable2.modelo.Producto;
import com.example.entregable2.modelo.Vendedor;
import com.example.entregable2.servicio.ServicioProducto;
import com.example.entregable2.servicio.ServicioVendedor;
import com.example.entregable2.servicio.impl.ServicioVendedorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/productos/{idProducto}/vendedores")
public class ControladorVendedoresProducto {

    @Autowired
    ServicioProducto servicioProducto;
    @Autowired
    ServicioVendedor servicioVendedor;

    @GetMapping
    public List<String> obtenerIdsVendedores(@PathVariable String idProducto){
        final Producto p = obtenerProductoPorId(idProducto);
        return p.idsVendedores;
    }

    public static class VendedorEntrada{
        public String idVendedor;
    }

    @DeleteMapping("/{idVendedor}")
    public void eliminarReferenciaVendedorProducto(@PathVariable String idProducto,
                                                   @PathVariable String idVendedor){

        final Producto x = obtenerProductoPorId(idProducto);

        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i< x.idsVendedores.size(); i++){
            if(x.idsVendedores.get(i).equals(idVendedor)){
                j = i;
                encontrado = true;
            }
        }

        if(encontrado){
            x.idsVendedores.remove(j);
            this.servicioProducto.reemplazarProducto(idProducto,x);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarReferenciaVendedorProducto(@PathVariable String idProducto,
                                                  @RequestBody VendedorEntrada v){

        final Producto x = obtenerProductoPorId(idProducto);
        final Vendedor y = this.servicioVendedor.obtenerVendedor(v.idVendedor);

        if(y==null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        for (String id: x.idsVendedores){
            if(id.equals(v.idVendedor))
                return; // 200 OK
        }

        x.idsVendedores.add(v.idVendedor);
        this.servicioProducto.reemplazarProducto(idProducto,x);
    }

    public Producto obtenerProductoPorId(String idProducto){
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

}
