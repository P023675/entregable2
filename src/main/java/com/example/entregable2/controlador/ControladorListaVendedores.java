package com.example.entregable2.controlador;

import com.example.entregable2.modelo.Vendedor;
import com.example.entregable2.servicio.ServicioVendedor;
import com.example.entregable2.servicio.impl.ServicioVendedorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/vendedores")
public class ControladorListaVendedores {

    @Autowired
    ServicioVendedor servicioVendedor;

    @PostMapping
    public void agregarVendedor(@RequestBody Vendedor v){
        this.servicioVendedor.agregarVendedor(v);
    }

    @GetMapping
    public List<Vendedor> obtenerVendedores(){
        return this.servicioVendedor.obtenerVendedores();
    }

}
