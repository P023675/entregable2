package com.example.entregable2.servicio.repos;

import com.example.entregable2.modelo.Vendedor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioVendedores extends MongoRepository<Vendedor,String> {
}
