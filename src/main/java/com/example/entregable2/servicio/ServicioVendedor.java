package com.example.entregable2.servicio;

import com.example.entregable2.modelo.Vendedor;

import java.util.List;

public interface ServicioVendedor {
    public String agregarVendedor(Vendedor v);
    public Vendedor obtenerVendedor(String id);
    public List<Vendedor> obtenerVendedores();
    public void reemplazarVendedor(String id, Vendedor v);
    public void eliminarVendedor(String id);
}
