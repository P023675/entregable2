package com.example.entregable2.servicio.impl;

import com.example.entregable2.modelo.Producto;
import com.example.entregable2.servicio.ServicioProducto;
import com.example.entregable2.servicio.repos.RepositorioProductos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProductos repositorioProductos;

    @Override
    public String agregarProducto(Producto p) {
        this.repositorioProductos.insert(p);
        return p.id;
    }

    @Override
    public Producto obtenerProducto(String id) {
        final Optional<Producto> p = this.repositorioProductos.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public List<Producto> obtenerProductos() {
        return this.repositorioProductos.findAll();
    }

    @Override
    public void reemplazarProducto(String id, Producto p) {
        p.id = id;
        this.repositorioProductos.save(p);
    }

    @Override
    public void eliminarProducto(String id) {
        this.repositorioProductos.deleteById(id);
    }
}
