package com.example.entregable2.servicio.impl;

import com.example.entregable2.modelo.Producto;
import com.example.entregable2.modelo.Vendedor;
import com.example.entregable2.servicio.ServicioVendedor;
import com.example.entregable2.servicio.repos.RepositorioVendedores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioVendedorImpl implements ServicioVendedor {

    @Autowired
    RepositorioVendedores repositorioVendedores;

    @Override
    public String agregarVendedor(Vendedor v) {
        this.repositorioVendedores.insert(v);
        return v.id;
    }

    @Override
    public Vendedor obtenerVendedor(String id) {
        final Optional<Vendedor> v = this.repositorioVendedores.findById(id);
        return v.isPresent() ? v.get() : null;
    }

    @Override
    public List<Vendedor> obtenerVendedores() {
        return this.repositorioVendedores.findAll();
    }

    @Override
    public void reemplazarVendedor(String id, Vendedor v) {
        v.id = id;
        this.repositorioVendedores.save(v);
    }

    @Override
    public void eliminarVendedor(String id) {
        this.repositorioVendedores.deleteById(id);
    }
}
