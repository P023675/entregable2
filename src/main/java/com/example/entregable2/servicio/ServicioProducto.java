package com.example.entregable2.servicio;

import com.example.entregable2.modelo.Producto;

import java.util.List;

public interface ServicioProducto {
    public String agregarProducto(Producto p);
    public Producto obtenerProducto(String id);
    public List<Producto> obtenerProductos();
    public void reemplazarProducto(String id, Producto p);
    public void eliminarProducto(String id);
}
